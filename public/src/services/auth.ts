/* eslint-disable class-methods-use-this */
import { Observable } from 'rxjs';
import firebase from 'firebase/app';
import { firestore, firebaseApp } from '../db';

class AuthService {
  private db = firestore();

  private allAuthEmitters: any[] = [];

  public listenForAuthToken(userId: string): Promise<string> {
    return new Promise((resolve, reject) => {
      this.db.collection('login')
        .doc(userId)
        .onSnapshot((doc: firebase.firestore.DocumentData) => {
          if (doc) {
            const data = doc.data();
            if (typeof data !== 'undefined') {
              if (typeof data.authtoken !== 'undefined') {
                resolve(data.authtoken);
              }
            }
          }
        }, (error: Error) => {
          reject(error.message);
        });
    });
  }

  public async signinWithToken(token: string): Promise<firebase.auth.UserCredential> {
    return new Promise((resolve, reject) => {
      firebaseApp().auth().signInWithCustomToken(token)
        .then((user: firebase.auth.UserCredential) => {
          resolve(user);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  public async signinWithStoredToken(): Promise<boolean> {
    const token = localStorage.getItem('auth-token');
    if (token === null) {
      return false;
    }
    try {
      await this.signinWithToken(token);
      return true;
    } catch (e) {
      console.warn(`Failed to signin with stored token: ${e}`);
      localStorage.removeItem('auth-token');
      return false;
    }
  }

  public requestChallenge(): Promise<string> {
    return new Promise((resolve, reject) => {
      firebaseApp().functions(process.env.VUE_APP_FIREBASE_REGION)
        .httpsCallable('request_challenge')()
        .then((res: firebase.functions.HttpsCallableResult) => {
          const challenge = res.data as string;
          resolve(challenge);
        })
        .catch((error: any) => {
          reject(error);
        });
    });
  }

  public signInAnonymously(): Promise<firebase.User> {
    return new Promise((resolve, reject) => {
      firebaseApp().auth().signInAnonymously()
        .then((credential: firebase.auth.UserCredential) => {
          if (credential.user) {
            resolve(credential.user);
          } else {
            reject(Error('User missing in credentials'));
          }
        })
        .catch((error) => {
          const errorMessage = error.message;
          reject(errorMessage);
        });
    });
  }

  public isAuthenticatedAnonymously(): Promise<Boolean> {
    const auth = firebaseApp().auth();

    return new Promise((resolve) => {
      if (auth && auth.currentUser !== undefined && auth.currentUser?.isAnonymous !== undefined) {
        const isSignedInAnonymously = auth.currentUser?.isAnonymous;
        resolve(isSignedInAnonymously);
      } else {
        resolve(false);
      }
    });
  }

  public isAuthenticated(): Boolean {
    const currentUser = firebaseApp().auth().currentUser?.uid != null;
    const isSignedInAnonymously = firebaseApp().auth().currentUser?.isAnonymous;
    return currentUser && !isSignedInAnonymously;
  }

  public getUserOrCrash(): firebase.User {
    const { currentUser } = firebaseApp().auth();
    if (typeof currentUser !== 'undefined' && currentUser !== null) {
      return currentUser;
    }
    throw Error('Cannot get user');
  }

  private getCurrentUser(): firebase.User |null {
    const { currentUser } = firebaseApp().auth();
    return currentUser;
  }

  public forceAuthEmit(emitters: any) {
    const currentUser = this.getCurrentUser();
    if (emitters) {
      emitters.forEach((e: any) => e.next(currentUser));
    } else {
      this.allAuthEmitters.forEach((e: any) => e.next(currentUser));
    }
  }

  public observeAuth(): Observable<firebase.User |null> {
    return Observable.create((emitter:any) => {
      this.allAuthEmitters.push(emitter);
      this.forceAuthEmit([emitter]);

      firebaseApp().auth().onAuthStateChanged((user) => {
        emitter.next(user);
      },
      (error) => {
        console.error(`Auth observer error: ${error}`);
      },
      () => {
        console.log('Auth observer removed');
      });
    });
  }

  public signOut() {
    localStorage.removeItem('auth-token');
    return firebaseApp().auth().signOut();
  }

  public authTokenExists(): boolean {
    return localStorage.getItem('auth-token') !== null;
  }

  /**
   * Get headers for making a API request. If user is authenticated,
   * this will include an authentication token for accessing API calls
   * that require authentication.
   */
  public async getRequestHeaders() {
    const headers: any = {
      Accept: 'application/json',
      'Content-Type': 'application/json;charset=UTF-8',
      cache: 'no-cache',
      mode: 'cors',
    };

    try {
      const authToken = await this.getUserOrCrash().getIdToken(true);
      headers.authorization = `Bearer ${authToken}`;
    } catch (e: any) {
      console.log(`Making un-authenticated request, auth not available: ${e}`);
    }
    return headers;
  }
}

const authService = new AuthService();

export default authService;
