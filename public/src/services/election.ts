import firebase from 'firebase/app';
import util from 'util';
import { function_url } from '../db';
import 'firebase/firestore';
import { IElection } from '../model/IElection';
import authService from './auth';

const electionUtil = {
  toIElection(electionData: firebase.firestore.DocumentData, electionId: string): IElection {
    return {
      content: electionData,
      id: electionId,
    } as IElection;
  },
};

// TODO: Move to typescript class?
const electionService = {
  async createElection(
    begin_height: number | null,
    contract_type: string,
    description: string,
    end_height: number,
    options: string[],
    participants: string[],
    visibility: string,
    tags: string[],
  ): Promise<string> {
    const url = function_url('create_election');
    const url_options = {
      method: 'POST',
      headers: await authService.getRequestHeaders(),
      body: JSON.stringify({
        begin_height: begin_height === -1 ? null : begin_height,
        contract_type,
        description,
        end_height,
        options,
        participants,
        visibility,
        tags,
      }),
    };
    const res: Response = await fetch(url, url_options);
    if (!res.ok) {
      throw Error(`Failed to create election: ${await res.text()}`);
    }
    const jsonRes = await res.json();
    return jsonRes.election_id;
  },

  async getElection(electionId: string): Promise<any> {
    const url = function_url(`get_election_details?election_id=${electionId}`);
    const url_options = {
      method: 'GET',
      headers: await authService.getRequestHeaders(),
    };
    const res: Response = await fetch(url, url_options);
    if (!res.ok) {
      throw Error(`Failed to request election details: ${await res.text()}`);
    }
    return res.json();
  },

  async getParticipantElections(current_user_id: string): Promise<Array<IElection>> {
    if (authService.getUserOrCrash().isAnonymous) {
      return [];
    }
    const url = function_url('list_participating_elections');
    const url_options = {
      method: 'GET',
      headers: await authService.getRequestHeaders(),
    };
    const res: Response = await fetch(url, url_options);
    if (!res.ok) {
      throw Error(`Failed to fetch election user is participating in: ${await res.text()}`);
    }

    return (await res.json()).elections.map((e: any) => electionUtil.toIElection(e, e.id));
  },

  async getAdminElections(current_user_id: string): Promise<Array<IElection>> {
    if (authService.getUserOrCrash().isAnonymous) {
      return [];
    }
    const url = function_url('list_hosted_elections');
    const url_options = {
      method: 'GET',
      headers: await authService.getRequestHeaders(),
    };
    const res: Response = await fetch(url, url_options);
    if (!res.ok) {
      throw Error(`Failed to fetch election user hosts: ${await res.text()}`);
    }

    return (await res.json()).elections.map((e: any) => electionUtil.toIElection(e, e.id));
  },

  async getTally(election_id: string): Promise<any> {
    const url = util.format('%s?election_id=%s',
      function_url('get_tally'),
      election_id);

    const options = {
      method: 'GET',
      headers: await authService.getRequestHeaders(),
    };

    const response: Response = await fetch(url, options);
    const json = await response.json();
    if (!response.ok) {
      throw Error(json.message);
    }
    return json;
  },
};

export default electionService;
