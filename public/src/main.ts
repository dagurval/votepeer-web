/* eslint-disable import/extensions */
import Vue from 'vue';
import {
  CarouselPlugin, BootstrapVue, IconsPlugin, LayoutPlugin,
} from 'bootstrap-vue';
import VueSlider from 'vue-slider-component';
import VueQrcodeReader from 'vue-qrcode-reader';
import App from './App.vue';
import router from './router';
import Store from './store';
import blockchain from './services/blockchain';
import 'vue-slider-component/theme/default.css';
import { normalizeAddress } from './validation';

// bootstrap css includes are in style/main.scss
import './style/main.scss';

const VueQrcode = require('@chenfengyuan/vue-qrcode');
const cashaddr = require('cashaddrjs');

Vue.use(VueQrcodeReader);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(LayoutPlugin);
Vue.use(CarouselPlugin);
Vue.config.productionTip = false;

/**
 * Global filters
 */

/**
 * Generates a title from a string.
 * First 40 chars of first line of string.
 */
Vue.filter('makeTitle', (description: string) => {
  const title = description.split('\n')[0];
  if (title.length <= 40) {
    return title;
  }
  return `${title.substring(0, 40)}...`;
});

/**
 * Generates a title from first line of a string.
 */
Vue.filter('makeTitleNoTruncate', (description: string) => description.split('\n')[0]);

/**
 * Estimates duration between block heights
 */
Vue.filter('estimateTime', (endHeight: number, currentHeight: number) => {
  if (endHeight < currentHeight) {
    return 'In the past';
  }
  if (endHeight === currentHeight) {
    return 'Now';
  }
  return blockchain.blockHeightToHuman(currentHeight, endHeight);
});

/**
 * Pretty print a BCH address.
 *
 * This "filter" produces HTML, so needs to be used like
 * <span v-html="ppAddress(address)"></span>
 */

Vue.mixin({
  methods: {
    ppAddress: (addressIn: string): string => {
      if (!addressIn) {
        return '';
      }
      // We don't escape the input, better to sanitize it.
      let address = '';
      try {
        address = normalizeAddress(addressIn);
      } catch (e) {
        return `ERROR: ${e}`;
      }
      const decoded = cashaddr.decode(address);

      const offset = decoded.prefix.length + 1; // + ":"

      const prefix = address.substr(offset, 4);
      const suffix = address.substr(address.length - 2);
      let middle = address.substr(
        offset + prefix.length,
        address.length - offset - prefix.length - suffix.length,
      );
      let hash = Buffer.from(decoded.hash).toString('hex');

      let html: string = `<span
        title="${address.substr(offset)}"
        style="font-family: 'Lucida Console', Monaco, monospace;">
        ${prefix}`;
      while (middle.length) {
        // color code using the hash (the rotate hash for next chunk).
        const color = hash.substr(0, 4);
        hash = hash.slice(4, hash.length) + color;

        const chunk = middle.slice(0, 4);
        middle = middle.slice(4);

        html += `<span style="
            background-color: #${color};
              letter-spacing: -6px;
              display: inline-block;
              width: 8px;
              height: 14px;
              overflow: hidden;
              padding: 0;
              line-height: 1;
              color : rgba(0, 0, 0, 0)
            ">${chunk}</span>`;
      }
      html += `${suffix}</span>`;
      return html;
    },
  },
});

export const viewModel = new Vue({
  render: (h) => h(App),
  router,
  store: Store,
}).$mount('#app');

Vue.component(VueQrcode.name, VueQrcode);
Vue.component('VueSlider', VueSlider);

export default viewModel;
