# VoterPeer web portal

This is the web portal for the VoterPeer project. The project is written with
Vue.js.

This project is deployed at <https://voter.cash>.

## Configuring backend

This project uses firebase as backend. The firebase functions are found in
`<parent directory>/functions`.

To configure firebase, see parent README.

## Project setup

`npm install`

### Compiles and hot-reloads for development

`npm run serve`

### Compiles and minifies for production

`npm run build`

### Lints and fixes files

`npm run lint`

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
