# VoterPeer

[![Chat on Telegram][ico-telegram]][link-telegram]
[![Twitter URL](https://img.shields.io/twitter/url/https/twitter.com/votepeer.svg?style=social&label=Follow%20%40votepeer)](https://twitter.com/votepeer)
[![MIT license](https://img.shields.io/github/license/Naereen/StrapDown.js.svg)](LICENSE.txt)
[![Website](https://img.shields.io/badge/Website_&_Demo-Online-green.svg)](https://voter.cash/)

## Overview

This project consists of:

1.  REST API over HTTPS implemented as firebase functions in the /functions
   folder

1.  A Vue.js application located in the /public folder

## Folder Structure
```dummy
/functions       # Node.js project (Hosted on firebase functions)
    /src
        index.ts # Node.js
/public          # Vue.js project (Hosted on firebase hosting)
voterpeer.conf   # Firebase hosting configuration
firebase.json    # Firebase hosting configuration
```

## One-time setup

- Install Node.js
- Set up a firebase project project

Log into firebase

`(cd functions; npm run login)`

To run the firebase emulator (`npm run emulate`) you also need Java installed.
On Debian/Ubuntu, run `sudo apt-get install openjdk-8-jdk`.

### Firebase configuration

Firebase is configured in `votepeer.conf`.

### For Mac users

Some of the npm dependencies may require xcode build tools.

`xcode-select --install`

## Build

To build both node server and vue application, run `npm run build`. These can
also be built seperately by running `npm run build` in the `functions` or
`public` folder.

### REST API Server

```bash
cd functions
npm run build
```

### Web portal in Vue

```bash
cd public
npm run build
```

## Running

### Run Vue.js locally

`(cd public; npm run serve)`

## Run functions locally
[Official docs](https://firebase.google.com/docs/functions/local-emulator)

Set up admin credentials for emulated functions:
1. Open the [Service Accounts pane](https://console.cloud.google.com/iam-admin/serviceaccounts)
of the Google Cloud Console.
2Make sure that App Engine default service account is selected, and use the
options menu at right to select Create key.
3. When prompted, select JSON for the key type, and click Create.

Set your Google default credentials to point to the downloaded key. Use the
path functions/keys to store the key, and make sure that it is in gitignore:

### Unix
```bash
export GOOGLE_APPLICATION_CREDENTIALS="<absolute_path>/functions/keys/<KeyFileName>.json"
npm run emulate
```

### Windows

```shell
set GOOGLE_APPLICATION_CREDENTIALS=functions\keys\<KeyFileName>.json
npm run emulate
```

## Testing functions

```bash
npm run emulate
npm run test
```

## Logs from all functions

`firebase functions:log`

## Deployment

`./deploy.sh`

## Development Tools

## Vue.js dashboard

For managing vue dependencies, profiling etc.

```bash
cd public
vue ui // Opens an dashboard for managing the vue.js client
```

## Pitfalls

Verify that:
`firebase-adminsdk-aboe1@<PROJECTID>.iam.gserviceaccount.com` and `<PROJECTID>@appspot.gserviceaccount.com`
has "Service Account Token Creator" permissions in google cloud dashboard
located
here: `https://console.cloud.google.com/iam-admin/iam?project=<PROJECTID>`

[ico-telegram]: doc/telegram.svg
[link-telegram]: https://t.me/buip129
