#!/usr/bin/env bash
set -e # exit on error
(cd functions; npm run build)
(cd public; npm run build)
