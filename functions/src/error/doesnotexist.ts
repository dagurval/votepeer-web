/**
 * Something requested does not exist
 */
export default class DoesNotExistError extends Error { }
