/**
 * Error thrown when a parameter to a API call has an invalid value.
 */
export default class InvalidParamError extends Error {
  constructor(param: string, details: string | null = null) {
    let msg = `Parameter '${param}' has invalid value`;
    if (details !== null) {
      msg += `. ${details}`;
    }
    super(msg);
  }
}
