it.todo('Resolve errors when running in CI, works locally');

// import { mockUserData } from "../../user/user.mock";
// import { mockAuth } from "../auth.mocks";
// const firebaseTest = require("@firebase/testing");
// import {AppOptions} from "firebase-admin/lib/firebase-namespace-api";
// import {getDbURL, getProjectId, getStorageBucket} from "../../cfg";
// import { IUser } from "../../user/model/user";
// const projectId = getProjectId();
//
// afterAll(() => {
//     firebaseTest.apps().forEach((app: any) => app.delete());
// });
//
// const projectConfig = {
//     databaseURL: getDbURL(),
//     projectId: getProjectId(),
//     storageBucket: getStorageBucket(),
// } as AppOptions;
// describe("Security rules", () => {
//
//     describe("anyone", () => {
//         let db: any = null;
//         db = firebaseTest.initializeTestApp(projectConfig).firestore();
//
//         it("cannot read /user/{<userId>}", async () => {
//             await firebaseTest.assertFails( db.doc(`user/<userId>`).get());
//         });
//         it("cannot write /user/{userId}", async () => {
//             const mockUser: IUser = mockUserData();
//             await firebaseTest.assertFails( db.doc(`user/<userId>`).set(mockUser));
//         });
//
//         it("cannot read /login/{userId}", async () => {
//             await firebaseTest.assertFails( db.doc(`login/<userId>`).get());
//         });
//         it("cannot write /login/{userId}", async () => {
//             const login = mockAuth();
//             await firebaseTest.assertFails( db.doc(`login/<userId>`).set(login));
//         });
//     });
//
//     describe("anonymous users", () => {
//         const aliceAuth = {
//             firebase: {
//                 sign_in_provider: "anonymous",
//             },
//             uid: "alice",
//         };
//         const db = firebaseTest.initializeTestApp({
//             auth: aliceAuth,
//             projectId,
//         }).firestore();
//
//         it("cannot read users/{userId}", async () => {
//             await firebaseTest.assertFails( db.doc(`user/${aliceAuth.uid}`).get());
//         });
//         it("cannot write /user/{userId}", async () => {
//             const mockUser: IUser = mockUserData();
//             await firebaseTest.assertFails( db.doc(`user/${aliceAuth.uid}`).set(mockUser));
//         });
//
//         it("can read login/{userId}", async () => {
//             await firebaseTest.assertSucceeds( db.doc(`login/${aliceAuth.uid}`).get());
//         });
//         it("cannot read login/{other_user_id}", async () => {
//             await firebaseTest.assertFails( db.doc(`login/other_user_id`).get());
//         });
//         it("cannot write /login/{userId}", async () => {
//             const mockUser: IUser = mockUserData();
//             await firebaseTest.assertFails( db.doc(`login/${aliceAuth.uid}`).set(mockUser));
//         });
//     });
//
//     describe("customly authenticated users", () => {
//         const aliceAuth = {
//             firebase: {
//                 sign_in_provider: "custom",
//             },
//             uid: "alice",
//         };
//
//         const db = firebaseTest.initializeTestApp({
//             auth: aliceAuth,
//             projectId,
//         }).firestore();
//
//         it("cannot read /login/{userId}", async () => {
//             await firebaseTest.assertFails( db.doc(`login/<userId>`).get());
//         });
//         it("cannot write /login/{userId}", async () => {
//             const login = mockAuth();
//             await firebaseTest.assertFails( db.doc(`login/<userId>`).set(login));
//         });
//
//         it("can read users/{userId}", async () => {
//             await firebaseTest.assertSucceeds( db.doc(`user/${aliceAuth.uid}`).get());
//         });
//         it("cannot read users/{other_user_id}", async () => {
//             await firebaseTest.assertFails( db.doc(`user/other_user_id`).get());
//         });
//         it("cannot write /users/{userId}", async () => {
//             const mockUser: IUser = mockUserData();
//             await firebaseTest.assertFails( db.doc(`users/${aliceAuth.uid}`).set(mockUser));
//         });
//     });
// });
