export interface IUser {
    id: string;
    fcmToken?: string;
    displayName?: string;
    imageUrl?: string;
}
