import randomstring from 'randomstring';
import bchaddrjs from 'bchaddrjs';
import { IAuth } from './auth/model/auth';

const bitcore = require('bitcore-lib');

export function generateChallenge(): string {
  return randomstring.generate();
}

export function verifySignature(msg: string, address: string, signature: string) {
  return new bitcore.Message(msg).verify(
    bchaddrjs.toLegacyAddress(address), signature,
  );
}

export function verifyAuthSignature(login: IAuth): boolean {
  return verifySignature(login.challenge, login.address, login.signature);
}
